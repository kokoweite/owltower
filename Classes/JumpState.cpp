//
//  JumpState.cpp
//  owlTower
//
//  Created by romain cadillon on 02/09/2015.
//
//

#include "JumpState.h"
#include "Character.h"
#include "AirState.h"
#include "StandState.h"

JumpState::JumpState()
{
}

CharacterState*
JumpState::handleInput(Input& input)
{
    return new AirState();
//    if (input.get_button() == Input::Button::JUMP_BUTTON) {
//        return new AirState();
//    }
//    else if (input.get_button() == Input::Button::RELEASE_BUTTON) {
//        return new StandState();
//    }

    return nullptr;
}

CharacterState* 
JumpState::update(Character& character)
{
    return nullptr;
}

void
JumpState::enter(Character& character)
{
    CCLOG("Enter JumpState");
    character.enterJumpState();
    character.jumpState();
}

void
JumpState::exit(Character& character)
{
    CCLOG("Exit JumpState");
}

JumpState::~JumpState()
{
    //    CCLOG("Delete JumState");
}