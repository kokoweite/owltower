//
//  GeomTools.cpp
//  KokoGame
//
//  Created by ro cad on 14/12/2014.
//
//

#include "GeomTools.h"

Vec2
GeomTools::MajorAxis(Vec2& v)
{
    if (fabsf(v.x) > fabsf(v.y)) {
        return Vec2(Sign(v.x), 0);
    } else {
        return Vec2(0, Sign(v.y));
    }
}

int
GeomTools::Sign(float a)
{
    return a > 0 ? 1 : -1;
}