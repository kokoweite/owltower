//
//  Animator.hpp
//  owlTower
//
//  Created by romain cadillon on 09/06/2016.
//
//

#ifndef Animator_hpp
#define Animator_hpp

#include <stdio.h>
#include "SpriteMovie.hpp"

class Animator{
    
private:
    cocos2d::Map<const char*, SpriteMovie*> _spriteMovieFromName;
    
public:
    Animator();
    void update(const cocos2d::Vec2& position);
    void addSpriteMovie(const char* key, std::unique_ptr<SpriteMovie>& spriteMovie);
    void play(const char* spriteMovieName, const char* animationName, bool isRepeated);
    
};
#endif /* Animator_hpp */
