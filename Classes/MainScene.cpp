//
//  MainScene.cpp
//  KokoGame
//
//  Created by ro cad on 11/08/2015.
//
//

#include "MainScene.h"
#include "WorldMap.h"
#include "KeyboardInput.h"
#include "TouchInput.h"
#include <iostream>

using namespace cocos2d;

MainScene::MainScene()
{

    // create a TMX map
    _world = new WorldMap("tilemap.tmx");
    Vec2 pos = Vec2(200.f, 150.f);

    //_keyboard_input = new KeyboardInput(this);
    _keyboard_input = new TouchInput(this);

    // Create main actor
    _hero = Owl::Builder(40, 62, pos, *_world)
              .setHasGravity(true)
              .setHasWorldCollision(true)
              .setHasFriction(true)
              .setOnGround(true)
              .build();

    // Add map to the current scene
    this->addChild(_world->get_map(), 0);
    float _x_offset = (Director::getInstance()->getVisibleSize().width -
                       (_world->getNbTileWidth() + 1) * _world->get_map()->getTileSize().width) /
                      2;

    _world->get_map()->setPosition(Vec2(_x_offset, 400.0));

    // Add main actor to the map
    _hero->addToWorld(_world->get_map());
    //_world->get_map()->addChild(_hero->get_sprite(), 0);

    // Setting up a game loop
    this->scheduleUpdate();
}

void
MainScene::update(float dt)
{

    _hero->handleInput(*_keyboard_input);
    //debug_input_handler();
    _hero->update(dt);
    _world->update(_hero->getVelocityY() * dt);
}

void
MainScene::debug_input_handler()
{
    if (_keyboard_input->get_button() == Input::DOWN_BUTTON)
        _hero->moveDown();

    if (_keyboard_input->get_button() == Input::UP_BUTTON)
        _hero->moveTop();

    if (_keyboard_input->get_button() == Input::LEFT_BUTTON)
        _hero->moveLeft();

    if (_keyboard_input->get_button() == Input::RIGHT_BUTTON)
        _hero->moveRigth();
}