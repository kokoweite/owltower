//
//  Owl.h
//  KokoGame
//
//  Created by ro cad on 03/08/2015.
//
//

#ifndef __KokoGame__Owl__
#define __KokoGame__Owl__

#include <stdio.h>
#include "Character.h"
#include "SpriteMovie.hpp"
#include "Animator.hpp"

class Owl : public Character
{

  private:
    float _fly;
    std::unique_ptr<SpriteMovie> _eyeSpriteMovie;
    std::unique_ptr<SpriteMovie> _owlSpriteMovie;
    std::unique_ptr<Animator> _animator;
    static const char* kEyesWinkAnimation;
    static const char* kOwlAnimation;
    static const char* kEyesAnimation;
    static const char* kOwlLeftAnimation;
    static const char* kOwlRightAnimation;
    static const char* kOwlSpriteMovie;
    static const char* kEyeSpriteMovie;

    /**
     * @brief Construct a new owl
     *
     * @param file Sprite file path
     * @param radius Owl radius
     */
    Owl();

    Owl(const unsigned int x_radius, const unsigned int y_radius, const cocos2d::Vec2& pos,
        WorldMap& map, const bool hasGravity, const bool hasWorldCollision, const bool hasFriction,
        const bool isDead, const bool onGround, const bool onLeft, const bool onRight,
        const bool onTop);

  public:
    // Destructor
    ~Owl();
    
    // use this class to construct Product
    class Builder;

    /**
     * @brief Calculate cumulated velocity which is used during the air state
     * Call update for the current state
     * Call update from super class
     *
     * @param dt Elapsed time since last frame
     */
    virtual void update(float dt);
    
    /**
     *
     */
    void addToWorld(cocos2d::TMXTiledMap* world);

    /**
     * @brief Handle user input. Manage character state
     *
     * @param input Manage all user input
     */
    void handleInput(Input& input);

    /**
     *
     */
    virtual void enterJumpState();

    /**
     * @brief Jump action. Called in JumpSate class
     */
    virtual void jumpState();
    
    /**
     *
     */
    virtual void enterAirState();

    /**
     * @brief In air action. Called in AirState class
     */
    virtual void airState();
    
    /**
     *
     */
    virtual void enterSlideRightState();
    
    /**
     *
     */
    virtual void exitSlideRightState();

    
    /**
     * @brief Implement what happen during SlideRightState
     */
    virtual void slideRightState();
    
    
    /**
     *
     */
    virtual void enterSlideLeftState();
    
    /**
     *
     */
    virtual void exitSlideLeftState();

    
    /**
     * @brief Implement what happen during SlideRightState
     */
    virtual void slideLeftState();
    
    /**
     * @brief Update the sprite position
     */
    virtual void updateSpritePosition(const cocos2d::Vec2& pos);

};

class Owl::Builder
{
  private:
    // variables needed for construction of object of Owl class
    cocos2d::Vec2 _velocity;
    float _max_y_velocity;
    unsigned int _xRadius;
    unsigned int _yRadius;
    float _groundFriction;
    bool _hasGravity;
    bool _hasFriction;
    bool _hasWorldCollision;
    bool _dead;
    bool _onGround;
    bool _onLeft;
    bool _onRigth;
    bool _onTop;
    cocos2d::Vec2& _pos;
    WorldMap& _map;

  public:
    // create Builder with default values assigned
    // (in C++11 they can be simply assigned above on declaration instead)
    Builder(const unsigned int xRadius, const unsigned int yRadius, cocos2d::Vec2& pos,
            WorldMap& map)
      : _xRadius(xRadius)
      , _yRadius(yRadius)
      , _pos(pos)
      , _map(map)
      , _groundFriction(0.6)
      , _hasGravity(false)
      , _hasFriction(false)
      , _hasWorldCollision(false)
      , _dead(false)
      , _onGround(false)
      , _onLeft(false)
      , _onRigth(false)
      , _onTop(false)
    {
    }

    // sets custom values for Product creation
    Builder& setXRadius(const unsigned int value)
    {
        this->_xRadius = value;
        return *this;
    }

    Builder& setYRadius(const unsigned int value)
    {
        this->_yRadius = value;
        return *this;
    }

    Builder& setPos(const cocos2d::Vec2& value)
    {
        this->_pos = value;
        return *this;
    }

    Builder& setMap(const WorldMap& value)
    {
        this->_map = value;
        return *this;
    }

    Builder& setGroundFriction(const float value)
    {
        this->_groundFriction = value;
        return *this;
    }

    Builder& setHasGravity(const bool value)
    {
        this->_hasGravity = value;
        return *this;
    }

    Builder& setHasFriction(const bool value)
    {
        this->_hasFriction = value;
        return *this;
    }

    Builder& setHasWorldCollision(const bool value)
    {
        this->_hasWorldCollision = value;
        return *this;
    }

    Builder& setDead(const bool value)
    {
        this->_dead = value;
        return *this;
    }

    Builder& setOnGround(const bool value)
    {
        this->_onGround = value;
        return *this;
    }

    Builder& setOnLeft(const bool value)
    {
        this->_onLeft = value;
        return *this;
    }

    Builder& setOnRigth(const bool value)
    {
        this->_onRigth = value;
        return *this;
    }

    Builder& setOnTop(const bool value)
    {
        this->_onTop = value;
        return *this;
    }

    // produce desired Product
    Owl* build()
    {
        // here optionaly check variable consistency
        // and also if Product is buildable from given information
        return new Owl(this->_xRadius, this->_yRadius, this->_pos, this->_map, this->_hasGravity,
                       this->_hasWorldCollision, this->_hasFriction, this->_dead,
                       this->_onGround, this->_onLeft, this->_onRigth, this->_onTop);
    }
};

#endif /* defined(__KokoGame__Owl__) */