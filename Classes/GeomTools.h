//
//  GeomTools.h
//  KokoGame
//
//  Created by ro cad on 14/12/2014.
//
//

#ifndef __KokoGame__GeomTools__
#define __KokoGame__GeomTools__

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class GeomTools
{

  public:
    /**
     @brief     calculate the major axis : x > y vec(x,0) | x < y vec(0,y)
     @param     vector on which calculate the major axis
     @return    major axis vector
     */
    static Vec2 MajorAxis(Vec2& v);

    /**
     * @param a Scalar to be signed
     * @return Signed value
     */
    static int Sign(float a);
};
#endif /* defined(__KokoGame__GeomTools__) */