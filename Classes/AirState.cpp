//
//  AirState.cpp
//  owlTower
//
//  Created by romain cadillon on 05/09/2015.
//
//

#include "AirState.h"
#include "Character.h"
#include "StandState.h"
#include "SlideLeftState.hpp"
#include "SlideRightState.hpp"
#include "JumpState.h"

AirState::AirState()
{
}

CharacterState*
AirState::handleInput(Input& input)
{
    if (input.get_button() == Input::Button::JUMP_BUTTON) {
        return new JumpState();
    }
    
    return nullptr;
}

CharacterState*
AirState::update(Character& character)
{
    character.airState();
    if (character.getCollideLeft()) {
        return new SlideLeftState();
    } else if (character.getCollideRight()) {
        return new SlideRightState();
    }

    return nullptr;
}

void
AirState::enter(Character& character)
{
    CCLOG("Enter AirState");
    character.enterAirState();
}

void
AirState::exit(Character& character)
{
    CCLOG("Exit AirState");
}

AirState::~AirState()
{
    //    CCLOG("Delete AirState");
}