//
//  SpriteMovie.cpp
//  owlTower
//
//  Created by romain cadillon on 03/10/2015.
//
//

#include "SpriteMovie.hpp"
#include <iostream>

using namespace cocos2d;

SpriteMovie::SpriteMovie(const std::string& plistFile)
  : Sprite::Sprite()
{
    auto spriteFrameCache = SpriteFrameCache::getInstance();
    spriteFrameCache->addSpriteFramesWithFile(plistFile);
}

SpriteMovie*
SpriteMovie::createSpriteMovie(const char* fileName, const std::string& plistFile)
{
    SpriteMovie* sm = new SpriteMovie(plistFile);

    if (sm->initWithSpriteFrameName(fileName)) {
        sm->autorelease();

        return sm;
    }

    CC_SAFE_DELETE(sm);

    return NULL;
}

void
SpriteMovie::addAnimation(const char* animationName, const char* file_name, unsigned int nb_frames,

                          float speed, bool reverse_animation)
{
    CCASSERT(nb_frames > 0, "nbFrame must be superior to 0");

    auto spriteFrameCache = SpriteFrameCache::getInstance();

    // Minus 1 because we do not want to add the first frame twice
    int totalFrames = (reverse_animation) ? nb_frames * 2 - 1 : nb_frames;
    Vector<SpriteFrame*> frames(totalFrames);

    if (nb_frames > 1) {
        char str[100] = { 0 };

        // Add frame to frames verctor
        for (int i = 1; i <= nb_frames; i++) {
            sprintf(str, file_name, i);
            frames.pushBack(spriteFrameCache->getSpriteFrameByName(str));
        }

        // If true, add frame in order to play reverse animation
        if (reverse_animation) {
            for (int i = nb_frames; i > 1; i--) {
                sprintf(str, file_name, i);
                frames.pushBack(spriteFrameCache->getSpriteFrameByName(str));
            }
        }
    } else {
        frames.pushBack(spriteFrameCache->getSpriteFrameByName(file_name));
    }

    // Create animation
    cocos2d::Animation* anim = Animation::createWithSpriteFrames(frames, speed);
    _actionFromName.insert(animationName, Animate::create(anim));
    _foreverActionName.insert(animationName, RepeatForever::create(Animate::create(anim)));
}

void
SpriteMovie::play(const char* animationName, bool isRepeated)
{
    this->stopAllActions();
    if (isRepeated)
        this->runAction(_foreverActionName.at(animationName));
    else
        this->runAction(_actionFromName.at(animationName));
}

void
SpriteMovie::stop()
{
    this->stopAction(_action);
}

SpriteMovie::~SpriteMovie()
{
}