//
//  SlideRightState.hpp
//  owlTower
//
//  Created by romain cadillon on 11/06/2016.
//
//

#ifndef SlideRightState_hpp
#define SlideRightState_hpp

#include <stdio.h>
#include "CharacterState.h"

class SlideRightState : public CharacterState
{
  public:
    SlideRightState();
    virtual ~SlideRightState();
    virtual CharacterState* handleInput(Input& input);
    virtual CharacterState* update(Character& character);
    virtual void enter(Character& character);
    virtual void exit(Character& character);
};
#endif /* SlideRightState_hpp */