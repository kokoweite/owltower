//
//  Collide.cpp
//  KokoGame
//
//  Created by ro cad on 30/11/2014.
//
//

#include "Collide.h"
#include <iostream>

bool
Collide::IsInternalCollision(int tileX, int tileY, const cocos2d::Vec2& normal, WorldMap& map,
                               AABB2& hero)
{

    int tileXHero = map.worldCoordToTileX(hero.getCentre().x);
    int tileYHero = map.worldCoordToTileX(hero.getCentre().y);

    // Get the next tile from the normal
    int nextTileX = tileX + normal.x;
    int nextTileY = tileY - normal.y;

    int predictedTileX = tileXHero - normal.x;
    int predictedTileY = map.getNbTileHeight() - tileYHero + normal.y;

    // Get next tile GID 0 = Not collidable 1 = Collidables
    unsigned int nextTileGID = map.getTileGIDAt(Vec2(nextTileX, nextTileY));
    unsigned int predictedTilePos = map.getTileGIDAt(Vec2(predictedTileX, predictedTileY));

    bool isInInt = false;

    if (!map.isTileObstacle(nextTileGID) && !map.isTileObstacle(predictedTilePos)) {

        float delta = 255;

        if (normal.y != 0) {
            if (tileX > predictedTileX) {
                delta =
                  hero.getCentre().x + hero.getHalfExtent().x - tileX * map.getTileWidth();
                isInInt = (delta >= -10 && delta <= 0) ? true : false;
            } else if (tileX < predictedTileX) {
                delta = hero.getCentre().x - hero.getHalfExtent().x -
                        ((tileX * map.getTileWidth()) + map.getTileWidth());
                isInInt = (delta <= 10 && delta >= 0) ? true : false;
            }
        } else {
            if (tileY > predictedTileY) {
                delta = hero.getCentre().y - hero.getHalfExtent().y -
                        ((map.getTileHeight() - tileY) * map.getTileHeight() +
                         map.getTileHeight());
                isInInt = (delta <= 10 && delta >= 0) ? true : false;
            } else if (tileY < predictedTileY) {
                delta = hero.getCentre().y + hero.getHalfExtent().y -
                        (((map.getTileHeight() - tileY) * map.getTileHeight()));
                isInInt = (delta >= -10 && delta <= 0) ? true : false;
            }
        }

        if (isInInt)
            return true;
    }

    return map.isTileObstacle(nextTileGID);
}

bool
Collide::AabbVsInternal(cocos2d::Vec2& delta, cocos2d::Vec2& tileCentre,
                          cocos2d::Vec2& aabbExtentsTile, cocos2d::Vec2& heroCentre,
                          Contact& outContact)
{

    // Form the closest plane normal to the point
    Vec2 planeNormal = GeomTools::MajorAxis(delta);
    planeNormal.negate();

    // Plane centre coordinates. plane of extend aabb
    Vec2 planeCentre =
      Vec2(planeNormal.x * aabbExtentsTile.x, planeNormal.y * aabbExtentsTile.y);
    planeCentre.add(tileCentre);

    // Distance point from plane
    Vec2 planeDelta = heroCentre - planeCentre;

    // Calculate the distance between the two aabb
    float dist = planeDelta.dot(planeNormal);

    // Init Contact object
    outContact.initialise(planeNormal, dist, heroCentre);

    return true;
}

bool
Collide::AabbVsAabb(AABB2& hero, AABB2& tile, Contact& outContact, int tile_i, int tile_j,
                      WorldMap& map, bool checkInternal)
{

    Vec2 aabbExtentTile = Vec2(tile.getHalfExtent());
    aabbExtentTile.add(hero.getHalfExtent());

    Vec2 delta = tile.getCentre() - hero.getCentre();

    Vec2 heroCentre = hero.getCentre();
    Vec2 tileCentre = tile.getCentre();

    AabbVsInternal(delta, tileCentre, aabbExtentTile, heroCentre, outContact);

    return !IsInternalCollision(tile_i, tile_j, outContact.getNormal(), map, hero);
}

bool
Collide::PointInAabb(cocos2d::Vec2& point, AABB2& aabb)
{

    Vec2 delta = point - aabb.getCentre();
    return fabsf(delta.x) < aabb.getHalfExtent().x && fabsf(delta.y) < aabb.getHalfExtent().y;
}