//
//  Collide.h
//  KokoGame
//
//  Created by ro cad on 30/11/2014.
//
//

#ifndef __KokoGame__Collide__
#define __KokoGame__Collide__

#include <stdio.h>
#include <cocos2d.h>
#include "WorldMap.h"
#include "Contact.h"
#include "GeomTools.h"
#include "AABB2.h"

class Collide
{

  public:
    /**
     * @brief Chech if internal collision occurs. i.e If collided against a
     * tile,
     * check if next tile from normal is collideable. Avoid ghost collision
     *
     * @param tile_x
     * @param tile_y
     * @param normal
     * @param map
     */
    static bool IsInternalCollision(int tileX, int tileY, const Vec2& normal, WorldMap& map,
                                      AABB2& hero);

    /**
     * @brief Calculate the plane normal, determine the distance between two
     * aabb
     *
     * @param delta
     * @param aabbCentre
     * @param aabbHalfExtents
     * @param point
     * @param outContact
     */
    static bool AabbVsInternal(Vec2& delta, Vec2& aabbCentre, Vec2& aabbHalfExtents, Vec2& point,
                                 Contact& outContact);

    /**
     * @brief
     *
     * @param hero
     * @param tile
     * @param out_contact
     * @param tile_i
     * @param tile_j
     * @param map
     * @param checkInternal
     */
    static bool AabbVsAabb(AABB2& hero, AABB2& tile, Contact& outContact, int tileI, int tileJ,
                             WorldMap& map, bool checkInternal = true);

    /**
     *
     */
    static bool PointInAabb(Vec2& point, AABB2& aabb);
};
#endif /* defined(__KokoGame__Collide__) */