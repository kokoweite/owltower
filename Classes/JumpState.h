//
//  JumpState.h
//  owlTower
//
//  Created by romain cadillon on 02/09/2015.
//
//

#ifndef __owlTower__JumpState__
#define __owlTower__JumpState__

#include <stdio.h>
#include "CharacterState.h"

class JumpState : public CharacterState
{
  public:
    JumpState();
    virtual ~JumpState();
    virtual CharacterState* handleInput(Input& input);
    virtual CharacterState*  update(Character& character);
    virtual void enter(Character& character);
    virtual void exit(Character& character);
};
#endif /* defined(__owlTower__JumpState__) */