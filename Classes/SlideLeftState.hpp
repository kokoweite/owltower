//
//  SlideLeftState.hpp
//  owlTower
//
//  Created by romain cadillon on 11/06/2016.
//
//

#ifndef SlideLeftState_hpp
#define SlideLeftState_hpp

#include <stdio.h>
#include "CharacterState.h"

class SlideLeftState : public CharacterState
{
  public:
    SlideLeftState();
    virtual ~SlideLeftState();
    virtual CharacterState* handleInput(Input& input);
    virtual CharacterState* update(Character& character);
    virtual void enter(Character& character);
    virtual void exit(Character& character);
};

#endif /* SlideLeftState_hpp */