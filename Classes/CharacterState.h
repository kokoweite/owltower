//
//  CharacterState.h
//  owlTower
//
//  Created by romain cadillon on 02/09/2015.
//
//

#ifndef __owlTower__CharacterState__
#define __owlTower__CharacterState__

class Character;
#include <stdio.h>
#include "Input.h"

class CharacterState
{
  public:
    /**
     * @brief Destroyed CharacterState instance
     */
    virtual ~CharacterState(){};

    /**
     * @brief Change to the right state given user input.
     * Asign
     *
     * @param input
     */
    virtual CharacterState* handleInput(Input& input) = 0;

    /**
     * @brief Action to perform during life of the current state
     *
     * @param character Character on which action is apply
     */
    virtual CharacterState* update(Character& character) = 0;

    /**
     * @brief Action to perform when enter in the current state
     *
     * @param character Character on which action is apply
     */
    virtual void enter(Character& character) = 0;

    /**
     * @brief Action to perform when exit the current state
     *
     * @param character Character on which action is apply
     */
    virtual void exit(Character& character) = 0;
};

#endif /* defined(__owlTower__CharacterState__) */