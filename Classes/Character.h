//
//  Characters.h
//  KokoGame
//
//  Created by ro cad on 04/08/2015.
//
//

#ifndef __KokoGame__Characters__
#define __KokoGame__Characters__

#include <stdio.h>
#include "MoveableObject.h"
#include "Input.h"
#include "CharacterState.h"

class Character : public MoveableObject
{

  protected:
    cocos2d::Vec2 _velTarget;
    AABB2 _tileAabb;
    float _yVeclocity;
    float _xVelocity;
    CharacterState* _characterState;
    int _life;
    /**
     * @brief Create a new character
     *
     * @param file Sprite sheet file which permit to create sprite
     */
    Character();

    Character(const unsigned int xRadius, const unsigned int yRadius, const cocos2d::Vec2& pos,
              WorldMap& map, const bool hasGravity, const bool hasWorldCollision,
              const bool hasFriction, const bool isDead, const bool onGround, const bool onLeft,
              const bool onRight, const bool onTop);

    void manageState(CharacterState* state);

  public:
    /**
     * @brief Update method.
     * Call super class (MoveableObject) update method
     *
     * @param dt Elapsed time since last frame
     */
    virtual void update(float dt);

    /**
     *
     */
    virtual void addToWorld(cocos2d::TMXTiledMap* world) = 0;

    /**
     * @brief Manage imput and final state machine
     *
     * @param input
     */
    virtual void handleInput(Input& input) = 0;

    /**
     *
     */
    virtual void enterJumpState() = 0;

    /**
     * @brief Implement initial impulse
     */
    virtual void jumpState() = 0;

    /**
     *
     */
    virtual void enterAirState() = 0;

    /**
     * @brief Implement what happen during jump
     */
    virtual void airState() = 0;

    /**
     *
     */
    virtual void enterSlideRightState() = 0;

    /**
     *
     */
    virtual void exitSlideRightState() = 0;

    /**
     * @brief Implement what happen during SlideRightState
     */
    virtual void slideRightState() = 0;

    /**
     *
     */
    virtual void enterSlideLeftState() = 0;

    /**
     *
     */
    virtual void exitSlideLeftState() = 0;

    /**
     * @brief Implement what happen during SlideRightState
     */
    virtual void slideLeftState() = 0;

    /**
     * Getter
     */
    inline cocos2d::Sprite* getSprite() const { return _sprite; }
    inline float getYVeclocity() const { return _yVeclocity; }
    inline float getXVelocity() const { return _xVelocity; }
    inline bool getOnFround() const { return _onGround; }
    inline int getLife() const { return _life; }

    /**
     * Setter
     */
    inline void setCharacter_state(CharacterState* characterState)
    {
        _characterState = characterState;
    }
    inline void setLife(int value) { _life = value; }

    /**
     * DEBUG
     */
    void moveRigth();
    void moveLeft();
    void moveTop();
    void moveDown();
};
#endif /* defined(__KokoGame__Characters__) */