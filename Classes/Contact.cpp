//
//  Contact.cpp
//  KokoGame
//
//  Created by ro cad on 23/11/2014.
//
//

#include "Contact.h"
using namespace cocos2d;

Contact::Contact()
  : _normal(0, 0)
  , _position(0, 0)
  , _dist(0)
  , _impluse(0)
{
}

Contact::Contact(Vec2& n, float dist, Vec2& position)
{
    initialise(n, dist, position);
}

void
Contact::initialise(cocos2d::Vec2& n, float dist, cocos2d::Vec2& position)
{
    _normal = n;
    _position = position;
    _dist = dist;
    _impluse = 0;
}