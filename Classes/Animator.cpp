//
//  Animator.cpp
//  owlTower
//
//  Created by romain cadillon on 09/06/2016.
//
//

#include "Animator.hpp"

using namespace cocos2d;

Animator::Animator()
{
}

void
Animator::update(const Vec2& position)
{
    //get all keys,stored in std::vector, that matches the object
    std::vector<const char*> mapKeyVec;
    mapKeyVec = _spriteMovieFromName.keys();
    for(auto key : mapKeyVec)
    {
         _spriteMovieFromName.at(key)->setPosition(position);
    }
}

void
Animator::addSpriteMovie(const char* spriteMovieName, std::unique_ptr<SpriteMovie>& spriteMovie)
{
    _spriteMovieFromName.insert(spriteMovieName, spriteMovie.get());
}

void
Animator::play(const char* spriteMovieName, const char* animationName, bool isRepeated)
{
    _spriteMovieFromName.at(spriteMovieName)->play(animationName);
}