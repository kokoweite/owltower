//
//  Owl.cpp
//  KokoGame
//
//  Created by ro cad on 03/08/2015.
//
//

#include "Owl.h"
#include <iostream>
#include "StandState.h"

using namespace cocos2d;

const char* Owl::kOwlAnimation{ "owlAnimation" };
const char* Owl::kOwlLeftAnimation{ "owlLeftAnimation" };
const char* Owl::kOwlRightAnimation{ "owlRightAnimation" };
const char* Owl::kEyesAnimation{ "eyesAnimation" };
const char* Owl::kOwlSpriteMovie{ "owlSpriteMovie" };
const char* Owl::kEyeSpriteMovie{ "eyeSpriteMovie" };
const char* Owl::kEyesWinkAnimation{ "eyesWinkAnimation" };

Owl::Owl()
  : Character()
{
}

Owl::Owl(const unsigned int x_radius, const unsigned int y_radius, const cocos2d::Vec2& pos,
         WorldMap& map, const bool hasGravity, const bool hasWorldCollision, const bool hasFriction,
         const bool isDead, const bool onGround, const bool onLeft, const bool onRight,
         const bool onTop)
  : Character(x_radius, y_radius, pos, map, hasGravity, hasWorldCollision, hasFriction, isDead,
              onGround, onLeft, onRight, onTop)
{
    _xVelocity = 400;
    _yVeclocity = 900;

    _owlSpriteMovie = std::unique_ptr<SpriteMovie>(SpriteMovie::createSpriteMovie("owl1.png", "testOwl.plist"));
    _owlSpriteMovie->addAnimation(kOwlAnimation, "owl%d.png", 7, 0.03, true);
    _owlSpriteMovie->addAnimation(kOwlRightAnimation, "owl-right-pan.png", 1);
    _owlSpriteMovie->addAnimation(kOwlLeftAnimation, "owl-left-pan.png", 1);
    
    _eyeSpriteMovie = std::unique_ptr<SpriteMovie>(SpriteMovie::createSpriteMovie("eyes-wink1.png", "eyes.plist"));
    _eyeSpriteMovie->addAnimation(kEyesWinkAnimation,"eyes-wink%d.png", 5, 0.05, false);
    
    _animator = std::unique_ptr<Animator>(new Animator());
    _animator->addSpriteMovie(kOwlSpriteMovie, _owlSpriteMovie);
    _owlSpriteMovie->play(kOwlAnimation);
    _eyeSpriteMovie->play(kEyesWinkAnimation);

    _sprite = _owlSpriteMovie.get();
    _sprite->setPosition(pos);
    _eyeSpriteMovie->setPosition(pos);

    _characterState = new StandState();
    _fly = 700;
}

void
Owl::update(float dt)
{
    manageState(_characterState->update(*this));
    Character::update(dt);
}

void
Owl::addToWorld(cocos2d::TMXTiledMap* world)
{
    world->addChild(_owlSpriteMovie.get(),0);
    world->addChild(_eyeSpriteMovie.get(),1);
}

void
Owl::handleInput(Input& input)
{
    manageState(_characterState->handleInput(input));
}


void
Owl::updateSpritePosition(const Vec2& position)
{
    _sprite->setPosition(position);
    _eyeSpriteMovie->setPosition(position);
}

void
Owl::enterJumpState()
{
}

void
Owl::jumpState()
{
    _velocity.x = 0;
    _velocity.x += _xVelocity;
    _velocity.y = 0;
    _velocity.y += _yVeclocity;
    _fly = 700;
}

void
Owl::enterAirState()
{
    _owlSpriteMovie->play(kOwlAnimation, false);
}

void
Owl::airState()
{
//    _fly -= (_fly - 25 > 0) ? 25 : _fly;
//    if (_fly > 0)
//        _velocity.y += 25;
}

void
Owl::enterSlideRightState()
{
    _owlSpriteMovie->play(kOwlRightAnimation, true);
    _xVelocity = fabsf(_xVelocity) * -1;
}

void
Owl::exitSlideRightState()
{
    _onRigth = false;
}

void
Owl::slideRightState()
{
}

void
Owl::enterSlideLeftState()
{
    _owlSpriteMovie->play(kOwlLeftAnimation, true);
    _xVelocity = fabsf(_xVelocity);
}

void
Owl::exitSlideLeftState()
{
    _onLeft = false;
}

void
Owl::slideLeftState()
{
}

Owl::~Owl()
{

}