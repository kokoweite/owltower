//
//  Characters.cpp
//  KokoGame
//
//  Created by ro cad on 04/08/2015.
//
//

#include "Character.h"
#include <iostream>

using namespace cocos2d;

Character::Character()
  : MoveableObject()
{
    _life = 5;
    _tileAabb = AABB2();
    _velTarget = Vec2();
    _characterState = nullptr;
}

Character::Character(const unsigned int x_radius, const unsigned int y_radius,
                     const cocos2d::Vec2& pos, WorldMap& map, const bool hasGravity,
                     const bool hasWorldCollision, const bool hasFriction, const bool isDead,
                     const bool onGround, const bool onLeft, const bool onRight, const bool onTop)
  : MoveableObject(x_radius, y_radius, pos, map, hasGravity, hasWorldCollision, hasFriction, isDead,
                   onGround, onLeft, onRight, onTop)
{
}

void
Character::manageState(CharacterState* state)
{
    if (state != nullptr) {
        _characterState->exit(*this);
        delete _characterState;
        _characterState = state;
        _characterState->enter(*this);
    }
}

void
Character::update(float dt)
{
    MoveableObject::update(dt);
}

void
Character::moveTop()
{
    _velocity.y += 20;
}

void
Character::moveRigth()
{
    _velocity.x += 20;
}

void
Character::moveLeft()
{
    _velocity.x -= 20;
}

void
Character::moveDown()
{
    _velocity.y -= 20;
}