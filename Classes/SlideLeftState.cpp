//
//  SlideLeftState.cpp
//  owlTower
//
//  Created by romain cadillon on 11/06/2016.
//
//

#include "SlideLeftState.hpp"
#include "Character.h"
#include "StandState.h"

SlideLeftState::SlideLeftState()
{
}

CharacterState*
SlideLeftState::handleInput(Input& input)
{
    if (input.get_button() == Input::Button::RELEASE_BUTTON) {
        return new StandState();
    }
    
    return nullptr;
}

CharacterState*
SlideLeftState::update(Character& character)
{
    return nullptr;
}

void
SlideLeftState::enter(Character& character)
{
    CCLOG("Enter SlideLeftState");
    character.enterSlideLeftState();
}

void
SlideLeftState::exit(Character& character)
{
    CCLOG("Exit SlideLeftState");
    character.exitSlideLeftState();
}

SlideLeftState::~SlideLeftState()
{
    //    CCLOG("Delete AirState");
}