//
//  IAABB.h
//  KokoGame
//
//  Created by ro cad on 16/11/2014.
//
//

#ifndef __KokoGame__AABB__
#define __KokoGame__AABB__

#include <stdio.h>
#include "cocos2d.h"

class AABB2
{

  protected:
    cocos2d::Vec2 _centre;
    cocos2d::Vec2 _halfExtent;

  public:
    AABB2();
    ~AABB2();
    AABB2(const cocos2d::Vec2& centre, const cocos2d::Vec2& halfHextent);

    void initialise(const cocos2d::Vec2& centre, const cocos2d::Vec2& halfHextent);
    cocos2d::Vec2 bottomLeft();
    cocos2d::Vec2 bottomRight();
    cocos2d::Vec2 topLeft();
    cocos2d::Vec2 topRight();
    cocos2d::Vec2& minInto(cocos2d::Vec2& v);
    void enlarge(float n);
    void enlargeY(float n);
    bool within(const cocos2d::Vec2& v);
    cocos2d::Vec2 clampInto(cocos2d::Vec2& v);
    void updateFrom(const cocos2d::Vec2& centre, const cocos2d::Vec2& halfHextent);
    cocos2d::Vec2 getMax();
    cocos2d::Vec2 getMin();

    // Getter
    inline cocos2d::Vec2& getCentre() { return _centre; }
    inline cocos2d::Vec2& getHalfExtent() { return _halfExtent; }

    // Setter
    inline void setCentre(const cocos2d::Vec2& value) { _centre = value; }
};
#endif /* defined(__KokoGame__AABB__) */