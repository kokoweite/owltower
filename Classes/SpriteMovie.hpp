//
//  SpriteMovie.hpp
//  owlTower
//
//  Created by romain cadillon on 03/10/2015.
//
//

#ifndef SpriteMovie_hpp
#define SpriteMovie_hpp

#include <stdio.h>

class SpriteMovie : public cocos2d::Sprite
{
  private:
    // std::vector<cocos2d::SpriteFrame*>* frames;
    cocos2d::Action* _action;
    cocos2d::Map<const char*, cocos2d::Action*> _actionFromName;
    cocos2d::Map<const char*, cocos2d::Action*> _foreverActionName;

    /**
     * @brief Create animation. This assume that animation has been created as
     * follow :
     *        "animation_name_1.extention"
     *        "animation_name_2.extention"
     *        and so on and so forth
     * @param
     */
    SpriteMovie(const std::string& plistFile);

  public:
    /**
     * @brief Factory method
     */
    static SpriteMovie* createSpriteMovie(const char* file_name, const std::string& plist_file);

    /**
     * @brief
     *
     * @param animationName
     * @param fileName
     * @param nbFrame
     * @param speed
     * @param reverseAnimation
     */
    void addAnimation(const char* animation_name, const char* file_name, unsigned int nb_frames,
                      float speed = 0.03, bool reverse_animation = false);

    /**
     * @brief Play animation
     *
     * @param animationName Animation's name that should be play ...
     */
    void play(const char* animation_name, bool isRepeated = true);

    /**
     * @brief Stop animation
     */
    void stop();

    /**
     * @brief Destructor
     */
    ~SpriteMovie();
};

#endif /* SpriteMovie_hpp */