//
//  AirState.h
//  owlTower
//
//  Created by romain cadillon on 05/09/2015.
//
//

#ifndef __owlTower__AirState__
#define __owlTower__AirState__

#include <stdio.h>
#include "CharacterState.h"

class AirState : public CharacterState
{
  public:
    AirState();
    virtual ~AirState();
    virtual CharacterState* handleInput(Input& input);
    virtual CharacterState* update(Character& character);
    virtual void enter(Character& character);
    virtual void exit(Character& character);
};
#endif /* defined(__owlTower__AirState__) */