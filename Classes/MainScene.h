//
//  MainScene.h
//  KokoGame
//
//  Created by ro cad on 11/08/2015.
//
//

#ifndef __KokoGame__MainScene__
#define __KokoGame__MainScene__

#include <stdio.h>
#include "WorldMap.h"
#include "Owl.h"
#include "Input.h"

class MainScene : public cocos2d::Scene
{

  private:
    WorldMap* _world;
    Owl* _hero;
    Input* _keyboard_input;

  public:
    MainScene();

    void update(float dt);

    void debug_input_handler();
};
#endif /* defined(__KokoGame__MainScene__) */