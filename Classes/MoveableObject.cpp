//
//  MoveableObject.cpp
//  KokoGame
//
//  Created by ro cad on 11/01/2015.
//
//

#include <iostream>
#include <math.h>
#include <algorithm>
#include "Collide.h"
#include "MoveableObject.h"

using namespace cocos2d;

MoveableObject::MoveableObject()
{

    _worldMap = nullptr;
    _velocity = Vec2();
    _posCorrect = Vec2(0, 0);
    _contact = new Contact();
    _maxYVelocity = 1200;
}

MoveableObject::MoveableObject(const unsigned int x_radius, const unsigned int y_radius,
                               const cocos2d::Vec2& pos, WorldMap& map, const bool hasGravity,
                               const bool hasWorldCollision, const bool hasFriction,
                               const bool isDead, const bool onGround, const bool onLeft,
                               const bool onRight, const bool onTop)
{
    _velocity = Vec2();
    _posCorrect = Vec2(0, 0);
    _contact = new Contact();
    _maxYVelocity = 1200;

    _halfExtent.x = x_radius;
    _halfExtent.y = y_radius;
    _worldMap = &map;
    _centre = pos;
    _hasGravity = hasGravity;
    _hasWorldCollision = hasWorldCollision;
    _hasFriction = hasFriction;
    _dead = isDead;
    _onGround = onGround;
    _onLeft = onLeft;
    _onRigth = onRight;
    _onTop = onTop;
    _groundFriction = 0.6;
}

void
MoveableObject::update(float dt)
{
    if (_hasGravity) {

        // Add gravity
        _velocity.y -= kGravity;

        // Clamp max speed
        _velocity.y = std::min(_velocity.y, _maxYVelocity);
    }

    if (_hasWorldCollision) {
        collision(dt);
    }

    Vec2 tmp = _velocity + _posCorrect;
    _centre.x += tmp.x * dt;
    _centre.y += tmp.y * dt;

    updateSpritePosition(_centre);

    _posCorrect.x = 0;
    _posCorrect.y = 0;
}

void
MoveableObject::innerCollision(AABB2& tileAabb, int i, int j, float dt)
{
    if (_worldMap->isTileObstacle(_worldMap->getTile(i, j))) {
        bool collided = Collide::AabbVsAabb(*this, tileAabb, *_contact, i, j, *_worldMap);
        if (collided) {
            modifyFriction(i, j);
            collisionResponse(_contact->getNormal(), _contact->getDistance(), dt);
        }
    }
}

void
MoveableObject::collision(float dt)
{
    // Vector prediction next frame
    Vec2 predictedPosition = Vec2(_centre);
    predictedPosition.x += _velocity.x * dt;
    predictedPosition.y += _velocity.y * dt;

    // Find min
    Vec2 min = Vec2(predictedPosition);
    min.x = std::min(min.x, _centre.x);
    min.y = std::min(min.y, _centre.y);

    // Find max
    Vec2 max = Vec2(predictedPosition);
    max.x = std::max(max.x, _centre.x);
    max.y = std::max(max.y, _centre.y);

    min.subtract(_halfExtent);
    max.add(_halfExtent);

    //    CCLOG("");
    //    CCLOG("--------- COLLISION ---------");
    //    CCLOG("POS     x = %f   y = %f", _centre.x, _centre.y);
    //    CCLOG("PREDPOS x = %f   y = %f", predictedPosition.x,
    //    predictedPosition.y);
    //    CCLOG("CENTRE  x = %d   y = %d",
    //    WorldMap::world_coord_to_tile_x(_centre.x),
    //    WorldMap::world_coord_to_tile_y(_centre.y) );
    //    CCLOG("MIN     x = %d   y = %d",
    //    WorldMap::world_coord_to_tile_x(min.x),
    //    WorldMap::world_coord_to_tile_y(min.y));
    //    CCLOG("MAX     x = %d   y = %d",
    //    WorldMap::world_coord_to_tile_x(max.x),
    //    WorldMap::world_coord_to_tile_y(max.y));
    //    CCLOG("-----------------------------");
    //    CCLOG("");

    //    max.add(Vec2(-1,0));
    min.subtract(Vec2(10, 10));
    max.add(Vec2(10, 10));
    _onGround = false;
    _worldMap->doActionToTilesWithinAabb(min, max, this, &MoveableObject::innerCollision,
                                               dt);
}

void
MoveableObject::modifyFriction(int i, int j)
{
    if (_worldMap->getTileGIDAt(Vec2(i, j)) == 1)
        _groundFriction = 0.1;
    else if (_worldMap->getTileGIDAt(Vec2(i, j)) == 2)
        _groundFriction = 0.6;
}

void
MoveableObject::collisionResponse(cocos2d::Vec2& normal, float distance, float dt)
{
    // Get the separation and penetration separately, this is to stop
    // penetration
    // from causing the objects to ping apart
    float separation = std::max(distance, 0.f);
    float penetration = std::min(0.f, distance);

    // Compute relative normal velocity require to the object to an exact stop
    // at the surface
    float nv = _velocity.dot(normal) + separation / dt;

    // Accumulate the penetration correction, this is applied in Update() and
    // ensure
    // we don't add any energy to the system
    _posCorrect.subtract(normal * (penetration / dt));

    if (nv < 0) {
        //
        //        if(sparation > 0 || penetration < 0)
        //        {
        //            CCLOG("");
        //            CCLOG("--------- COLLISION RESPONSE ---------");
        //            CCLOG("%f %f",_centre.x, _centre.y);
        //            CCLOG("Velocity     x = %f  y = %f", _velocity.x,
        //            _velocity.y);
        //            CCLOG("normal       x = %f  y = %f", normal.x, normal.y);
        //            CCLOG("separration    = %f", sparation);
        //            CCLOG("penetration    = %f", penetration);
        //            CCLOG("dt             = %f", dt);
        //            CCLOG("separation/dt  = %f", sparation/dt);
        //            CCLOG("penetration/dt = %f", penetration/dt);
        //            CCLOG("vel.normal     = %f", _velocity.dot(normal));
        //            CCLOG("nv             = %f", nv);
        //            CCLOG("normal * nv  x = %f   y = %f", (normal * nv).x,
        //            (normal * nv).y);
        //            CCLOG("---------------------------------------");
        //            CCLOG("");
        //        }

        // Remove normal velocity
        _velocity.subtract(normal * nv);

        if (normal.y > 0) {

            onCollideDown();

            if (_hasFriction) {

                // Get the tangent from normal (perpendicular vector)
                Vec2 tangent = normal.getPerp();

                // Compute the tangential velocity, scale y friction
                float tv = _velocity.dot(tangent) * _groundFriction;

                _velocity.subtract(tangent * tv);

                if (!_onGroundLast) {
                    // Landing transition function goes here
                }
            }

        } else if (normal.y < 0) {
            onCollideTop();
        } else if (normal.x > 0) {
            onCollideLeft();
        } else if (normal.x < 0) {
            onCollideRight();
        }
    }
}

void
MoveableObject::onCollideLeft()
{
    CCLOG("on_collide_left");
    _onLeft = true;
}

void
MoveableObject::onCollideRight()
{
    CCLOG("on_collide_right");
    _onRigth = true;
}

void
MoveableObject::onCollideDown()
{
    _onGround = true;
}

void
MoveableObject::onCollideTop()
{
    _onTop = true;
}

MoveableObject::~MoveableObject()
{
    CCLOG("MoveableObject destructor");
    delete _contact;
    _worldMap = nullptr;
}