//
//  UserInput.h
//  KokoGame
//
//  Created by ro cad on 25/08/2015.
//
//

#ifndef __KokoGame__UserInput__
#define __KokoGame__UserInput__

#include <stdio.h>
#include "Input.h"

class KeyboardInput : public Input{
    
public:
    KeyboardInput(cocos2d::Scene* scene);
    void on_key_pressed(cocos2d::EventKeyboard::KeyCode key_code, cocos2d::Event* event);
    void on_key_released(cocos2d::EventKeyboard::KeyCode key_code, cocos2d::Event* event);
};
#endif /* defined(__KokoGame__UserInput__) */
