//
//  Map.cpp
//  KokoGame
//
//  Created by ro cad on 23/11/2014.
//
//

#include "WorldMap.h"
#include <iostream>

USING_NS_CC;

const Vec2 WorldMap::tileCentreOffset{ 48, 48 };
const Vec2 WorldMap::tileHalfExtents{ 48, 48 };

WorldMap::WorldMap(const std::string& tmxFile)
{

    // Create the map from file
    _map = TMXTiledMap::create(tmxFile);

    // Get main layer
    _main_layer = _map->getLayer("foreground");

    // Get heigth
    _nbTileY = _main_layer->getLayerSize().height - 1;

    // Get width
    _nbTileX = _main_layer->getLayerSize().width - 1;

    _aabbTemp = AABB2();

    _tileWidth = _map->getTileSize().width;
    _tileHeight = _map->getTileSize().height;
    //
    //    for (int i = 0; i < 10; i++) {
    //        auto rectNode = DrawNode::create();
    //        Vec2 rectangle[4];
    //        rectangle[0] = Vec2(0, 32);
    //        rectangle[1] = Vec2(32,32);
    //        rectangle[2] = Vec2(32, 0);
    //        rectangle[3] = Vec2(0, 0);
    //
    //        Color4F white(1, 1, 1, 1);
    //        rectNode->drawPolygon(rectangle, 4, white, 1, white);
    //        rectNode->setAnchorPoint(Vec2(0.5, 0.5));
    //        _map->addChild(rectNode);
    //        _debugQuad[i] = rectNode;
    //    }
}

void
WorldMap::update(float delta)
{
    float y = _map->getPositionY();
    y -= delta;
    _map->setPositionY(y);
}

Sprite*
WorldMap::getTileAt(const cocos2d::Vec2& tileCoordinate)
{
    return _main_layer->getTileAt(tileCoordinate);
}

unsigned int
WorldMap::getTile(int i, int j)
{
    return getTileGIDAt(Vec2(i, j));
}

unsigned int
WorldMap::getTileGIDAt(const cocos2d::Vec2& tileCoordinate, TMXTileFlags* flags)
{

    if (tileCoordinate.x > 0 && tileCoordinate.x <= _nbTileX && tileCoordinate.y >= 0 &&
        tileCoordinate.y <= _nbTileY) {
        return _main_layer->getTileGIDAt(tileCoordinate, flags);
    } else
        return kInvalid;
}

bool
WorldMap::isTileObstacle(unsigned int gid)
{
    return gid > 0;
}

int
WorldMap::tileCoordToWorldX(int i)
{

    return i * kTileWidth;
}

int
WorldMap::tileCoordToWorldY(int j)
{

    return j * kTileWidth;
}

int
WorldMap::worldCoordToTileX(float x)
{
    return x / kTileWidth;
}

int
WorldMap::worldCoordToTileY(float y)
{
    return y / kTileWidth;
}

void
WorldMap::FillInTileAabb(int i, int j, AABB2& out_aabb, int nbTileY)
{
    Vec2 vector = Vec2(tileCoordToWorldX(i), tileCoordToWorldY(nbTileY - j));
    vector.add(tileCentreOffset);
    out_aabb.initialise(vector, tileHalfExtents);
}

void
WorldMap::doActionToTilesWithinAabb(cocos2d::Vec2& min, cocos2d::Vec2& max, MoveableObject* mo,
                                         void (MoveableObject::*action)(AABB2&, int, int, float),
                                         float dt)
{

    int minI = worldCoordToTileX(min.x);
    int minJ = _nbTileY - worldCoordToTileX(min.y);

    int maxI = worldCoordToTileX(max.x);
    int maxJ = _nbTileY - worldCoordToTileY(max.y);

    //    int k = 0;

    for (int i = minI; i <= maxI; i++) {
        for (int j = maxJ; j <= minJ; j++) {

            //            if(k<10)
            //            {
            //                _debugQuad[k]->setPosition(i*32, (_nb_tile_y-j) *
            //                32);
            //            }
            //            k++;

            // Generate aabb for the current tile
            FillInTileAabb(i, j, _aabbTemp, _nbTileY);

            // call the delegate on the main collision map
            (*mo.*action)(_aabbTemp, i, j, dt);
        }
    }
}

WorldMap::~WorldMap()
{
    CCLOG("WorldMap destructor");
}