#include "AppDelegate.h"
#include "MainScene.h"

USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(1242, 2208);
static cocos2d::Size smallResolutionSize = cocos2d::Size(320, 480);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(768, 1024);
static cocos2d::Size largeResolutionSize = cocos2d::Size(1242, 2208);

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate()
{
}

// if you want a different context,just modify the value of glContextAttrs
// it will takes effect on all platforms
void
AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributions,now can only set six attributions:
    // red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = { 8, 8, 8, 8, 24, 8 };

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages,
// don't modify or remove this function
static int
register_all_packages()
{
    return 0; // flag for packages manager
}

bool
AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if (!glview) {
        glview = GLViewImpl::create("My Game");
        glview->setFrameSize(960, 800);
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);

    // Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width,
                                    designResolutionSize.height,
                                    ResolutionPolicy::FIXED_WIDTH);

    Size frameSize = glview->getFrameSize();

    // if the frame's height is larger than the height of medium size.
    if (frameSize.width > mediumResolutionSize.width) {
        director->setContentScaleFactor(
          MIN(largeResolutionSize.width / designResolutionSize.width,
              largeResolutionSize.width / designResolutionSize.width));
        CCLOG("---1-->%f", director->getContentScaleFactor());
    }
    // if the frame's height is larger than the height of small size.
    else if (frameSize.width > smallResolutionSize.width) {
        director->setContentScaleFactor(
          MIN(mediumResolutionSize.width / designResolutionSize.width,
              mediumResolutionSize.width / designResolutionSize.width));
        CCLOG("---2-->%f", director->getContentScaleFactor());
    }
    // if the frame's height is smaller than the height of medium size.
    else {
        director->setContentScaleFactor(
          MIN(smallResolutionSize.width / designResolutionSize.width,
              smallResolutionSize.width / designResolutionSize.width));
        CCLOG("---3-->%f", director->getContentScaleFactor());
    }

    register_all_packages();

    MainScene* mainScene = new MainScene();

    // run
    director->runWithScene(mainScene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone
// call,it's be invoked too
void
AppDelegate::applicationDidEnterBackground()
{
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void
AppDelegate::applicationWillEnterForeground()
{
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}