//
//  MoveableObject.h
//  KokoGame
//
//  Created by ro cad on 11/01/2015.
//
//

#ifndef __KokoGame__MoveableObject__
#define __KokoGame__MoveableObject__

#include <stdio.h>
#include "Contact.h"
#include "AABB2.h"
#include "WorldMap.h"

class MoveableObject : public AABB2
{

  protected:
    // Attributes
    cocos2d::Sprite* _sprite;
    WorldMap* _worldMap;
    Contact* _contact;
    cocos2d::Vec2 _posCorrect;
    cocos2d::Vec2 _velocity;
    float _maxYVelocity;
    float _groundFriction;
    bool _hasGravity;
    bool _hasFriction;
    bool _hasWorldCollision;
    bool _dead;
    bool _onGround;
    bool _onGroundLast;
    bool _onLeft;
    bool _onRigth;
    bool _onTop;

    /**
     * @brief Construct a new MoveableObject initialized with a new sprite and a
     * radius
     *
     * @param file Sprite's file name
     * @param radius Radius of the MoveableObject
     */
    MoveableObject();

    MoveableObject(const unsigned int x_radius, const unsigned int y_radius,
                   const cocos2d::Vec2& pos, WorldMap& map, const bool hasGravity,
                   const bool hasWorldCollision, const bool hasFriction, const bool isDead,
                   const bool onGround, const bool onLeft, const bool onRight, const bool onTop);

    /**
     * @brief Calculate the collision response by computing relative normal
     * velocity for
     * an excact stop at the surface
     *
     * @param normal Normal at the surface
     * @param distance Distance between two AABB entities (i.e tile & hero)
     * @param dt Elapsed time since last frame
     */
    void collisionResponse(cocos2d::Vec2& normal, float distance, float dt);

    /**
     * @brief Check if there is a collision between hero's AABB and tile's AABB
     * If there is a collision launch collision response
     *
     * @param tile_aabb Tile's AABB
     * @param i Tile coordinate on x
     * @param j Tile coordinate on y
     * @param dt Elapsed time since last frame
     */
    void innerCollision(AABB2& tile_aabb, int i, int j, float dt);

    /**
     * @brief Update the sprite position
     */
    virtual void updateSpritePosition(const cocos2d::Vec2& pos) = 0;

    /**
     * @brief Modify friction value depending on tile you collide with
     *
     * @param i Tile coordinate on x
     * @param j Tile coordinate on y
     */
    void modifyFriction(int i, int j);

  public:
    /**
     * @brief Destructor
     */
    ~MoveableObject();

    /**
     * @brief Calculate the predicted position and define tile interval on
     * which collision should be apply
     *
     * @param dt Elapsed time since last frame
     */
    void collision(float dt);

    /**
     * @brief Compute gravity and call collision function
     *
     * @param dt Elapsed time since last frame
     */
    virtual void update(float dt);

    /**
     * Called when left collision occurs
     */
    virtual void onCollideLeft();

    /**
     * Called when left collision occurs
     */
    virtual void onCollideRight();

    /**
     * Called when left collision occurs
     */
    virtual void onCollideDown();

    /**
     * Called when left collision occurs
     */
    virtual void onCollideTop();

    /**
     * Setter
     */
    inline void setPosition(cocos2d::Vec2& position) { _centre = position; }
    inline void setDead(bool value) { _dead = value; };

    /**
     * Getter
     */
    inline bool getDead() const { return _dead; }
    inline bool getHasFriction() const { return _hasFriction; }
    inline bool getHasWorldCollision() const { return _hasWorldCollision; }
    inline bool getHasGravity() const { return _hasGravity; }
    inline float getVelocityY() const { return _velocity.y; }
    inline cocos2d::Vec2 getPosition() const { return _centre; }
    inline bool getCollideLeft() const { return _onLeft; }
    inline bool getCollideRight() const { return _onRigth; }
    inline bool getCollideTop() const { return _onTop; }
    inline bool getCollideGround() const { return _onGround; }
    

    /**
     * Const
     */
    static constexpr float kGravity{ 40.f };
    static constexpr float kMaxSpeed{ 200.f };
};
#endif /* defined(__KokoGame__MoveableObject__) */