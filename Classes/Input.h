//
//  Input.h
//  KokoGame
//
//  Created by ro cad on 27/08/2015.
//
//

#ifndef __KokoGame__Input__
#define __KokoGame__Input__

#include <stdio.h>

class Input{
    
public:
    enum Button{
        JUMP_BUTTON,
        RELEASE_BUTTON,
        ATTACK_BUTTON,
        LEFT_BUTTON,
        RIGHT_BUTTON,
        UP_BUTTON,
        DOWN_BUTTON
    };
    
    Input();
    
    Button get_button() const;
    
protected:
    
    Button _button;
};
#endif /* defined(__KokoGame__Input__) */
