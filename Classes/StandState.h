//
//  StandState.h
//  owlTower
//
//  Created by romain cadillon on 02/09/2015.
//
//

#ifndef __owlTower__StandState__
#define __owlTower__StandState__

#include <stdio.h>
#include "CharacterState.h"

class StandState : public CharacterState
{
  public:
    StandState();
    virtual ~StandState();
    virtual CharacterState* handleInput(Input& input);
    virtual CharacterState* update(Character& character);
    virtual void enter(Character& character);
    virtual void exit(Character& character);
};
#endif /* defined(__owlTower__StandState__) */