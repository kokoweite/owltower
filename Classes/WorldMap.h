//
//  Map.h
//  KokoGame
//
//  Created by ro cad on 23/11/2014.
//
//

#ifndef __KokoGame__Map__
#define __KokoGame__Map__

#include <stdio.h>
#include "AABB2.h"

//#include "MoveableObject.h"
class MoveableObject;

class WorldMap
{

  private:
    cocos2d::TMXTiledMap* _map;
    cocos2d::TMXLayer* _main_layer;
    int _nbTileX;
    int _nbTileY;
    int _tileWidth;
    int _tileHeight;
    AABB2 _aabbTemp;

    static const cocos2d::Vec2 tileCentreOffset;
    static const cocos2d::Vec2 tileHalfExtents;

    // Debug
    cocos2d::DrawNode* _debugQuad[10];

  public:
    WorldMap(const std::string& tmxFile);
    ~WorldMap();

    void update(float delta);

    /**
     * Check if there are collision between aabb between min and max and the
     * player
     *
     * @param min Minimum tile coordinate interval
     * @param max Maximum tile coordinate interval
     * @param dt
     */
    void doActionToTilesWithinAabb(cocos2d::Vec2& min, cocos2d::Vec2& max, MoveableObject* mo,
                                        void (MoveableObject::*functer)(AABB2& tileAabb, int i,
                                                                        int j, float dt),
                                        float dt);

    /**
     * @brief Determine whether tile is obstacle or not
     *
     * @param tile's gid
     */
    bool isTileObstacle(unsigned int gid);

    /**
     * @brief Transform y axis tile coordinate to y axis world coordinate
     *
     * @param j Tile coord along y axis
     */
    static int tileCoordToWorldY(int j);

    /**
     * @brief Transform x axis tile coordinate to x axis coordinate
     *
     * @param x Tile coord along x axis
     */
    static int tileCoordToWorldX(int i);

    /**
     * @brief Transform x world coordinate to x tile coordinate
     *
     * @param x World coordinate along x axis
     */
    static int worldCoordToTileX(float x);

    /**
     * @brief
     *
     * @param
     */
    static int worldCoordToTileY(float y);

    /**
     * @brief
     *
     * @param
     */
    static void FillInTileAabb(int i, int j, AABB2& out_aabb, int nb_tile_y);

    unsigned int getTileGIDAt(const cocos2d::Vec2& tileCoordinate,
                                 cocos2d::TMXTileFlags* flags = nullptr);
    unsigned int getTile(int i, int j);
    cocos2d::Sprite* getTileAt(const cocos2d::Vec2& tileCoordinate);

    /**
     * Getter
     */
    inline cocos2d::TMXTiledMap* get_map() const { return _map; }
    inline unsigned int getNbTileHeight() const { return _nbTileY; }
    inline unsigned int getNbTileWidth() const { return _nbTileX; }
    inline unsigned int getTileWidth() const { return _tileWidth; }
    inline unsigned int getTileHeight() const { return _tileHeight; }

    // Const
    static constexpr unsigned int kInvalid{ 0xff };
    static constexpr unsigned int kEmpty{ 0x00 };
    static constexpr unsigned int kTileWidth{ 96 };
};

#endif /* defined(__KokoGame__Map__) */