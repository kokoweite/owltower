//
//  AABB.cpp
//  KokoGame
//
//  Created by ro cad on 16/11/2014.
//
//

#include "AABB2.h"
#include <math.h>
#include <iostream>
USING_NS_CC;

AABB2::AABB2()
  : _centre(0, 0)
  , _halfExtent(0, 0)
{
    CCLOG("AABB2 default constructor");
}

AABB2::AABB2(const Vec2& centre, const Vec2& halfExtent)
{
    CCLOG("--> %f ---> %f", halfExtent.x, halfExtent.y);
    initialise(centre, halfExtent);
}

void
AABB2::initialise(const Vec2& centre, const Vec2& half_hextent)
{
    _centre = centre;
    _halfExtent = half_hextent;

    if (!(_halfExtent.x >= 0 && _halfExtent.y >= 0))
        CCLOG("AABB2::initialize: Cannot have negative half extents! ");
}

Vec2
AABB2::bottomLeft()
{
    return Vec2(_centre.x - _halfExtent.x, _centre.y - _halfExtent.y);
}

Vec2
AABB2::bottomRight()
{
    return Vec2(_centre.x + _halfExtent.x, _centre.y - _halfExtent.y);
}

Vec2
AABB2::topLeft()
{
    return Vec2(_centre.x - _halfExtent.x, _centre.y + _halfExtent.y);
}

Vec2
AABB2::topRight()
{
    return Vec2(_centre.x + _halfExtent.x, _centre.y + _halfExtent.y);
}

Vec2&
AABB2::minInto(cocos2d::Vec2& v)
{
    v.set(_centre);
    v.subtract(_halfExtent);
    return v;
}

void
AABB2::enlarge(float n)
{
    _halfExtent.x += n;
    _halfExtent.y += n;
}

void
AABB2::enlargeY(float n)
{
    _halfExtent.y += n;
}

bool
AABB2::within(const cocos2d::Vec2& v)
{
    Vec2 w = Vec2(fabsf(v.x - _centre.x), fabsf(v.y - _centre.y));
    return w.x < _halfExtent.x && w.y < _halfExtent.y;
}

Vec2
AABB2::clampInto(Vec2& v)
{
    v -= _centre;
    v.clamp(getMin(), getMax());
    v += _centre;
    return v;
}

void
AABB2::updateFrom(const cocos2d::Vec2& centre, const cocos2d::Vec2& half_hextent)
{
    _centre = centre;
    _halfExtent = half_hextent;
}

Vec2
AABB2::getMax()
{
    return bottomRight();
}

Vec2
AABB2::getMin()
{
    return topLeft();
}

AABB2::~AABB2()
{
    CCLOG("AABB2 destructor");
}