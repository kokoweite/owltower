//
//  StandState.cpp
//  owlTower
//
//  Created by romain cadillon on 02/09/2015.
//
//

#include "StandState.h"
#include "Character.h"
#include "JumpState.h"
#include <iostream>

StandState::StandState()
{
}

CharacterState*
StandState::handleInput(Input& input)
{
    if (input.get_button() == Input::Button::JUMP_BUTTON) {
        return new JumpState();
    }
//    else if (input.get_button() == Input::Button::RELEASE_BUTTON) {
//        return new StandState();
//    }

    return nullptr;
}

CharacterState* 
StandState::update(Character& character)
{
    return nullptr;
}

void
StandState::enter(Character& character)
{
    //    CCLOG("Enter StandState");
}

void
StandState::exit(Character& character)
{
    //    CCLOG("Exit StandState");
}

StandState::~StandState()
{
    //    CCLOG("Delete StandState");
}