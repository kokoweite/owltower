//
//  TouchInput.cpp
//  owlTower
//
//  Created by romain cadillon on 11/09/2015.
//
//

#include "TouchInput.h"

using namespace cocos2d;

TouchInput::TouchInput(cocos2d::Scene* scene)
{

    //  Create a "one by one" touch event listener
    // (processes one touch at a time)

    auto listener1 = EventListenerTouchOneByOne::create();

    // trigger when you push down
    listener1->onTouchBegan = [&](Touch* touch, Event* event) {
        //_button = Button::JUMP_BUTTON;
        return true; // if you are consuming it
    };

    // trigger when moving touch
    listener1->onTouchMoved = [](Touch* touch, Event* event) {
        // your code
    };

    // trigger when you let up
    listener1->onTouchEnded = [=](Touch* touch, Event* event) { _button = Button::RELEASE_BUTTON; };

    auto listener2 = EventListenerTouchAllAtOnce::create();
    // trigger when you let up
    listener2->onTouchesEnded = [=](std::vector<Touch*> touch, Event* event) { _button = Button::RELEASE_BUTTON; };
    listener2->onTouchesBegan = [&](std::vector<Touch*> touch, Event* event) {
        
        if (touch.size() == 2) {
            CCLOG("ATTACK");
            _button = Button::ATTACK_BUTTON;
        } else if (touch.size() == 1) {
            CCLOG("JUMP");
            _button = Button::JUMP_BUTTON;
        }

        return true; // if you are consuming it
    };

    // Add listener
    //scene->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener1, scene);
    scene->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener2, scene);
}