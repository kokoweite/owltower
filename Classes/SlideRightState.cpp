//
//  SlideLeftState.cpp
//  owlTower
//
//  Created by romain cadillon on 11/06/2016.
//
//

#include "SlideRightState.hpp"
#include "Character.h"
#include "StandState.h"

SlideRightState::SlideRightState()
{
}

CharacterState*
SlideRightState::handleInput(Input& input)
{
    if (input.get_button() == Input::Button::RELEASE_BUTTON) {
        return new StandState();
    }
    
    return nullptr;
}

CharacterState*
SlideRightState::update(Character& character)
{
    return nullptr;
}

void
SlideRightState::enter(Character& character)
{
    CCLOG("Enter SlideRightState");
    character.enterSlideRightState();
}

void
SlideRightState::exit(Character& character)
{
    CCLOG("Exit SlideRightState");
    character.exitSlideRightState();
}

SlideRightState::~SlideRightState()
{
    //    CCLOG("Delete AirState");
}