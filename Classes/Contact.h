//
//  Contact.h
//  KokoGame
//
//  Created by ro cad on 23/11/2014.
//
//

#ifndef __KokoGame__Contact__
#define __KokoGame__Contact__

#include <stdio.h>
#include <cocos2d.h>

class Contact
{

  private:
    cocos2d::Vec2 _normal;
    cocos2d::Vec2 _position;
    float _dist;
    float _impluse;

  public:
    /**
     * @brief Construct a new contact object
     */
    Contact();

    /**
     * @brief Construct a new contact object
     *
     * @param n Normal
     * @param dist Distance between two AABB
     * @param p Hero Position
     */
    Contact(cocos2d::Vec2& n, float dist, cocos2d::Vec2& p);

    /**
     * @brief Inialize contact object
     *
     * @param n Normal
     * @param dist Distance between two AABB
     * @param p Hero Position
     */
    void initialise(cocos2d::Vec2& n, float dist, cocos2d::Vec2& p);

    /**
     * Getter
     */
    inline cocos2d::Vec2& getNormal() { return _normal; }
    inline cocos2d::Vec2& getPosition() { return _position; }
    inline float getDistance() const { return _dist; }
    inline float getImpulse() const { return _impluse; }
};
#endif /* defined(__KokoGame__Contact__) */