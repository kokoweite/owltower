//
//  UserInput.cpp
//  KokoGame
//
//  Created by ro cad on 25/08/2015.
//
//

#include "KeyboardInput.h"

using namespace cocos2d;

KeyboardInput::KeyboardInput(Scene* scene) : Input(){
    
    // creating a keyboard event listener
    auto listener = EventListenerKeyboard::create();
    listener->onKeyPressed = CC_CALLBACK_2(KeyboardInput::on_key_pressed,this);
    listener->onKeyReleased = CC_CALLBACK_2(KeyboardInput::on_key_released, this);
    
    scene->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, scene);
}


// Implementation of the keyboard event callback function prototype
void KeyboardInput::on_key_pressed(EventKeyboard::KeyCode key_code, Event* event){
    
    if(key_code == EventKeyboard::KeyCode::KEY_SPACE)
        _button = Button::JUMP_BUTTON;
    else if(key_code == EventKeyboard::KeyCode::KEY_LEFT_ARROW)
        _button = Button::LEFT_BUTTON;
    else if(key_code == EventKeyboard::KeyCode::KEY_RIGHT_ARROW)
        _button = Button::RIGHT_BUTTON;
    else if(key_code == EventKeyboard::KeyCode::KEY_DOWN_ARROW)
        _button = Button::DOWN_BUTTON;
    else if(key_code == EventKeyboard::KeyCode::KEY_UP_ARROW)
        _button = Button::UP_BUTTON;

}


// Implementation of the keyboard event callback function prototype
void KeyboardInput::on_key_released(EventKeyboard::KeyCode keyCode, Event* event){
    
    //if(keyCode == EventKeyboard::KeyCode::KEY_SPACE)
    CCLOG("RELEASE SPACE");
    _button = Button::RELEASE_BUTTON;
}
