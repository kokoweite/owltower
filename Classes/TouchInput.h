//
//  TouchInput.h
//  owlTower
//
//  Created by romain cadillon on 11/09/2015.
//
//

#ifndef __owlTower__TouchInput__
#define __owlTower__TouchInput__

#include <stdio.h>
#include "Input.h"

class TouchInput : public Input{
    
public:
    TouchInput(cocos2d::Scene* scene);
};
#endif /* defined(__owlTower__TouchInput__) */
